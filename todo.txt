- thumbnails for jquery-fileupload ui

- establish the way pathing works for image saving
    - share this information with team for back-end integration
    
- stop autouploading
    - https://github.com/blueimp/jQuery-File-Upload/wiki/Basic-plugin#how-to-start-uploads-with-a-button-click
    - made a post here: (http://www.alpacajs.org/docs/fields/upload.html)
    